/*
 * Part of chuika - cheapest tourists radio.
 * Here is ws2812 rgb led access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../stm32/delay.h"
#include "ws2812.h"

// local functions
void setLine(void);
void resetLine(void);
void sendBit(uint8_t byte, int n);
void sendByte(uint8_t byte);

void wsInit()
{
    gpioSetPushPull(WS_PORT, WS_PIN);
}

void setLine()
{
    gpioSet(WS_PORT, WS_PIN);
}

void resetLine(void)
{
    gpioReset(WS_PORT, WS_PIN);
}

void sendBit(uint8_t byte, int n)
{
    if((byte & (1 << n)) == 0) {
        setLine();
        rough_delay_us(T0H);
        resetLine();
        rough_delay_us(T0L);
    } else {
        setLine();
        rough_delay_us(T1H);
        resetLine();
        rough_delay_us(T1L);
    }
}

void sendByte(uint8_t byte)
{
    // MSB first
    for(int i=8 ; i>=0 ; --i) {
        sendBit(byte,i);
    }
}

// global functions
void wsResetLine()
{
    resetLine();
    delay_ms(RES_MS);
    setLine();
}

void wsUpdateNoob(uint8_t red, uint8_t green, uint8_t blue)
{
    sendByte(red);
    sendByte(green);
    sendByte(blue);
}

void wsUpdate(wsRgbTyp *rgb)
{
    sendByte(rgb->red);
    sendByte(rgb->green);
    sendByte(rgb->blue);
}

void wsUpdateCascade(wsRgbTyp *rgb, int size)
{
    wsResetLine();
    for(int i=0 ; i<size ; ++i) {
        wsUpdate(&rgb[i]);
    }
    wsResetLine();
}
