#ifndef H_LM75
#define H_LM75
/*
 * Part of chuika - cheapest tourists radio.
 * Here is lm75 digital thermometer register macro and functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

void lm75Init(void);
void lm75On(void);
void lm75Off(void);
int lm75ReadTemp(void);
int lm75ReadTempShutdown(void);

#define LM75_ADDR   0x90

/* Configuration register */
#define LM75_CONF   0x01
#define LM75_CONF_DEFAULT   0x00
// OS fault queue programming
#define OS_F_QUE_1  0x00
#define OS_F_QUE_2  0x08
#define OS_F_QUE_4  0x10
#define OS_F_QUE_6  0x18
// OS polarity selection
#define OS_POL_LOW  0x00
#define OS_POL_HIGH 0x04
// OS operation mode selection
#define OS_COMP_INT 0x02
// device operation mode selection
#define SHUTDOWN    0x01

/* Temperature register */
#define LM75_TEMP   0x00
#define TEMP_EXTRACT(reg1,reg2) \
    (uint16_t)( (((uint16_t)reg1&0x00ff) << 3) + (((uint16_t)reg2&0x00ff) >> 5) )

/* Overtemperature shutdown threshold register */
#define LM75_TOS    0x03
#define TOS_SHIFT   6

/* Hysteresis register */
#define LM75_THYST  0x02

#define TEMP_CONVERSION_PERIOD  105

#endif
