#ifndef H_PT2313_REG
#define H_PT2313_REG
/*
 * Part of chuika - cheapest tourists radio.
 * Here is digital filter pt2313 register macro.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define PT2313_ADDRESS  0x88

/* Volume control register */
// very userful formula
#define VOLUME_PERCENT(n)   ((uint8_t)(63-(n*0.63)))
// unused reg def from datasheet
#define VOLUME_DB(db)   \
    ((uint8_t)( (((db/10)<<3) & 0x31) || ((db/1.25)&0x07) ))
#define VOLUME_0DB      0x00
#define VOLUME_M1P25DB  0x01
#define VOLUME_M2P5DB   0x02
#define VOLUME_M3P75DB  0x03
#define VOLUME_M5DB     0x04
#define VOLUME_M6P25DB  0x05
#define VOLUME_M7P5DB   0x06
#define VOLUME_M8P75DB  0x07
#define VOLUME_M10DB    0x08
#define VOLUME_M20DB    0x10
#define VOLUME_M30DB    0x18
#define VOLUME_M40DB    0x20
#define VOLUME_M50DB    0x28
#define VOLUME_M60DB    0x30
#define VOLUME_M70DB    0x38

/* Speaker attenuation registers */
// speaker choice
#define SPEAKER_LF  0x80
#define SPEAKER_RF  0xa0
#define SPEAKER_LR  0xc0
#define SPEAKER_RR  0xe0
// very userful formula
#define ATTENUATION_PERCENT(n)  ((uint8_t)(31-(n*0.31)))
// unused reg def from datasheet
#define ATTENUATION_DB(db)  \
    ((uint8_t)( ( (((db/10) << 3) & 0x11) || ((db/1.25) & 0x07) ) ))
#define ATTENUATION_0DB      0x00
#define ATTENUATION_M1P25DB  0x01
#define ATTENUATION_M2P5DB   0x02
#define ATTENUATION_M3P75DB  0x03
#define ATTENUATION_M5DB     0x04
#define ATTENUATION_M6P25DB  0x05
#define ATTENUATION_M7P5DB   0x06
#define ATTENUATION_M8P75DB  0x07
#define ATTENUATION_M10DB    0x08
#define ATTENUATION_M20DB    0x10
#define ATTENUATION_M30DB    0x18
#define ATTENUATION_MUTE     0x1f

/* Audio switch register */
// input ports
#define STEREO1         0x40
#define STEREO2         0x41
#define STEREO3         0x42
#define STEREO4         0x43
// loudness
#define LOUDNESS_ON     0x00
#define LOUDNESS_OFF    0x04
// input gain
#define GAIN11P25DB     0x00
#define GAIN7P5DB       0x08
#define GAIN3P75DB      0x10
#define GAIN0DB         0x18

/* Bass and treble control registers */
// bass or treble
#define BASS    0x60
#define TREBLE  0x70
// tone control in db
#define TONE_DB(db)     ((uint8_t)(((14-db)/2)&0x07))
#define TONE_MDB(db)    ((uint8_t)(((-1*(14-db)/2)+8)&0x0f))
#define TONE_M14DB  0x00
#define TONE_M12DB  0x01
#define TONE_M10DB  0x02
#define TONE_M8DB   0x03
#define TONE_M6DB   0x04
#define TONE_M4DB   0x05
#define TONE_M2DB   0x06
#define TONE_0DB    0x07
#define TONE_00DB   0x0f
#define TONE_2DB    0x0e
#define TONE_4DB    0x0d
#define TONE_6DB    0x0c
#define TONE_8DB    0x0b
#define TONE_10DB   0x0a
#define TONE_12DB   0x09
#define TONE_14DB   0x08


#endif
