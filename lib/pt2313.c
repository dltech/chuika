/*
 * Part of chuika - cheapest tourists radio.
 * Here is digital filter pt2313 functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stm32/i2c.h"
#include "pt2313.h"

uint8_t lastSwitch;

int pt2313WriteReg(uint8_t reg)
{
    return i2cTxByte(PT2313_ADDRESS, reg);
}

void pt2313Bass(int db)
{
    if(db<0) {
        pt2313WriteReg(BASS | TONE_MDB(db));
    } else {
        pt2313WriteReg(BASS | TONE_DB(db));
    }
}

void pt2313Treble(int db)
{
    if(db<0) {
        pt2313WriteReg(TREBLE | TONE_MDB(db));
    } else {
        pt2313WriteReg(TREBLE | TONE_DB(db));
    }
}

void pt2313Att(uint8_t speaker, int percent)
{
    pt2313WriteReg(speaker | ATTENUATION_PERCENT(percent));
}

void pt2313Volume(int percent)
{
    pt2313WriteReg(VOLUME_PERCENT(percent));
}

void pt2313Mute()
{
    pt2313WriteReg(lastSwitch | LOUDNESS_ON);
}

void pt2313MuteOff()
{
    pt2313WriteReg(lastSwitch | LOUDNESS_OFF);
}

void pt2313InputSelect(int number, uint8_t gain)
{
    switch(number)
    {
        case 1:
            lastSwitch = STEREO1 | gain;
            break;
        case 2:
            lastSwitch = STEREO2 | gain;
            break;
        case 3:
            lastSwitch = STEREO3 | gain;
            break;
        case 4:
            lastSwitch = STEREO4 | gain;
            break;
    }
    pt2313WriteReg(lastSwitch | LOUDNESS_OFF);
}
