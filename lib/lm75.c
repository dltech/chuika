/*
 * Part of chuika - cheapest tourists radio.
 * Here is lm75 digital thermometer functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "lm75.h"
#include "../stm32/i2c.h"
#include "../stm32/delay.h"

int calcTemp(uint8_t reg1, uint8_t reg2);
uint16_t calcTos(int temp);
void writeConfReg(uint8_t reg);

void writeConfReg(uint8_t reg)
{
    uint8_t txData[2];
    txData[0] = LM75_CONF;
    txData[1] = reg;
    i2cTxArray(LM75_ADDR, txData, 2);
}

void lm75Init()
{
    uint8_t txData[2];
    txData[0] = LM75_CONF;
    txData[1] = LM75_CONF_DEFAULT | SHUTDOWN;
    i2cTxArray(LM75_ADDR, txData, 2);
}

void lm75On()
{
    writeConfReg(LM75_CONF_DEFAULT);
}

void lm75Off()
{
    writeConfReg(LM75_CONF_DEFAULT | SHUTDOWN);
}

int lm75ReadTemp()
{
    i2cTxByte(LM75_ADDR, LM75_TEMP);
    uint8_t rxData[2];
    i2cRxArray(LM75_ADDR, rxData, 2);
    return calcTemp(rxData[0], rxData[1]);
}

int lm75ReadTempShutdown()
{
    lm75On();
    delay_ms(TEMP_CONVERSION_PERIOD);
    int temp = lm75ReadTemp();
    lm75Off();
    return temp;
}

int calcTemp(uint8_t reg1, uint8_t reg2)
{
    uint16_t reg = TEMP_EXTRACT(reg1, reg2);
    if((reg & 0x0400) > 0) {
        return (-1000*((~reg)+1))/8;
    } else {
        return (1000*((int)reg))/8;
    }
}

/**
 * @function
 * Calc hysteresis register temparature value
 * @param temp - temperature, multiplied by 1000
 * @return 16-bit data with MSB and LSB TOS registers.
 */
uint16_t calcTos(int temp)
{
    if(temp < 0) {
        return ((~((-2*temp)/1000))+0x101) << TOS_SHIFT;
    } else {
        return ((2*temp)/1000) << TOS_SHIFT;
    }
}
