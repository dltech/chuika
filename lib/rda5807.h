#ifndef H_RDA5807
#define H_RDA5807
/*
 * Part of chuika - cheapest tourists radio.
 * RDA5807 digital rf chip access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "rda5807_reg.h"

#define ON  1
#define OFF 0

void rdaInitConfig(void);
// access to RDA chip
int rdaUpdateConfig(void);
int rdaRead(void);

// properties

// on/off options
void rdaPowerOn(uint8_t onOff);
void rdaReset(uint8_t onOff);
void rdaOutputHiz(uint8_t onOff);

// sound settings
void rdaMute(uint8_t onOff);
void rdaMute2(uint8_t onOff)
void rdaMono(uint8_t onOff);
void rdaBass(uint8_t onOff);
void rdaVolume(uint8_t vol);

// receiver options
void rdaAfc(uint8_t onOff);
void rdaDeEmphasis50(uint8_t onOff);
void rdaNewDemodulator(uint8_t onOff);
// softblend filter
void rdaFilter(uint8_t onOff);
void rdaSoftBlendThresholdDb(uint8_t db);

// seek
void rdaSeek(uint8_t onOff);
void rdaTune(uint8_t onOff);
// seek options
void rdaEndlessSeek(uint8_t onOff);
void rdaSeekDir(uint8_t onOff);
void rdaSeekThreshold(uint8_t snr);
// old seek mode
void rdaOldSeekMode(uint8_t onOff);
void rdaOldSeekThreshold(uint8_t snr);
// search progress
int isRdaSeekComplete(void);
int isRdaSeekFail(void);

// clocking config
void rdaExternalClocking(void);
void rdaClkFreq(int kHz);
void rdaQuartz(uint8_t onOff);
void rdaCalibrate(uint8_t onOff);
void rdaClockInvert(uint8_t onOff);

// radio frequency
// fm frequency band choosing
void rdaBand50_65(void);
void rdaBand65_76(void);
void rdaBand76_91(void);
void rdaBand87_108(void);
void rdaBand76_108(void);
void rdaChannelSpacing(int kHz);
// channel choosing
void rdaChannelN(uint16_t n);
void rdaChannelKhz(uint32_t khz);
// direct access to the frequency
void rdaFreqDirectMode(uint8_t onOff);
void rdaFreqDirect(uint32_t khz);
// current shannel after seek
uint16_t rdaGetChannel(void);

// input LNA
void rdaLnaCurrentUa(uint16_t current);

// rds
void rdaGetRds(uint16_t *rds);
// rds options
void rdaRdsEn(uint8_t onOff);
void rdaRdsFifoMode(uint8_t onOff);
void rdaRdsFifoClear(uint8_t onOff);
void rdaRbds(uint8_t onOff);
// rds statement
int isRdaRdsReady(void);
int isRdaRdsSynchronized(void);
int rdaGetRdsErrors(void);

// fm station condition
int isRdaStereo(void);
int isRdaRdsTrueStation(void);
int isRdaRdsReady(void);
uint8_t rdaGetRssi(void);



#define RDA_ADDRESS     0x20
#define TX_REGS         6
#define RX_REGS         6
#define RDS_BLOCK_SIZE  4

// register type definition
typedef union {
    struct {
        uint8_t lowByte;
        uint8_t hiByte;
    };
    uint16_t reg;
}  rdaRegTyp;

enum {
    REG02H = 0,
    REG03H,
    REG04H,
    REG05H,
    REG06H,
    REG07H,
    REG08H,
    REG09H
};

enum {
    REG0AH = 0,
    REG0BH,
    REG0CH,
    REG0DH,
    REG0EH,
    REG0FH
};

#endif
