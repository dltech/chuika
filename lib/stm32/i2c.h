#ifndef H_I2C
#define H_I2C
/*
 * Part of chuika - cheapest tourists radio.
 * Here is I2C access functions.
 *
 * Copyright 2022 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "regs/gpio_regs.h"

#define I2C_PORT    GPIOB_CRL
#define I2C_PIN1    6
#define I2C_PIN2    7

#define I2C_TOUT    1e4


void i2cInit(void);
int i2cTxByteInner(uint8_t byte);
int i2cTxAddr(uint8_t addr);
int i2cTxStop(void);
int i2cRxByteInner(void);

int i2cTxByte(uint8_t addr, uint8_t data);
int i2cTxArray(uint8_t addr, uint8_t *data, int size);
int i2cRxByte(uint8_t addr);
int i2cRxArray(uint8_t addr, uint8_t *data, int size);

#endif
