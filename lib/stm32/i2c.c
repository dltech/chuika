/*
 * Part of chuika - cheapest tourists radio.
 * Here is I2C access functions.
 *
 * Copyright 2022 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "regs/rcc_regs.h"
#include "regs/i2c_regs.h"
#include "i2c.h"

void i2cInit()
{
    // module clocking
    RCC_APB1ENR |= I2C1EN;
    RCC_APB2ENR |= IOPBEN;
//    RCC_AHBENR  |= DMA1EN;
    // port config
    I2C_PORT = CNF_AF_OPEN_DRAIN(I2C_PIN1) | MODE_OUTPUT50(I2C_PIN1)\
             | CNF_AF_OPEN_DRAIN(I2C_PIN2) | MODE_OUTPUT50(I2C_PIN2);
    // clocking of the interface
    I2C1_CR2 = ITERREN | (36 & FREQ_MSK) | IDMAEN;
    I2C1_CCR = F_S | DUTY | I2C400K;
    I2C1_TRISE = TRISE_NS(50);
    I2C1_OAR1 = WTF;
    // i2c enable
    I2C1_CR1 = PE;

    // // dma not used
    // // tx channel
    // DMA1_CPAR6 = (uint32_t) &I2C1_DR;
    // DMA1_CMAR6 = (uint32_t) &flashToUsbBuffer;
    // DMA1_CNDTR6 = (uint32_t) I2C_BUFFER_SIZE;
    // DMA1_CCR6 = MINC | MSIZE_8BIT | PSIZE_8BIT | PL_VERY_HIGH | TEIE | DIR | TCIE;
    // NVIC_EnableIRQ(DMA1_Channel6_IRQn);
    //
    // // rx channel
    // DMA1_CPAR7 = (uint32_t) &I2C1_DR;
    // DMA1_CMAR7 = (uint32_t) &flashToUsbBuffer;
    // DMA1_CNDTR7 = (uint32_t) I2C_BUFFER_SIZE;
    // DMA1_CCR7 = MINC | MSIZE_8BIT | PSIZE_8BIT | PL_VERY_HIGH | TEIE;
    // NVIC_EnableIRQ(DMA1_Channel7_IRQn);
}


int i2cTxAddrInner(uint8_t addr)
{
    // wait for a busy state of the interface
    int32_t tOut = I2C_TOUT;
    while( ((I2C1_SR2 & BUSY) != 0) && (--tOut > 0) );
    if(tOut <= 0) {
        return -1;
    }
    I2C1_SR1 = 0;
    (void)I2C1_SR2;
    (void)I2C1_SR1;
    // sending start bit with control sequence
    I2C1_CR1 |= START;
    tOut = I2C_TOUT;
    while( ((I2C1_SR1 & SB) == 0 ) && (--tOut > 0) );
    if(tOut <= 0) {
        return -1;
    }
    // sending address with control sequence
    I2C1_DR = addr;
    (void)I2C1_SR1;
    tOut = I2C_TOUT;
    while( ((I2C1_SR1 & ADDR) == 0) && ((I2C1_SR1 & AF) == 0) && (--tOut > 0) );
    if(tOut <= 0) {
        return -1;
    }
    if( (I2C1_SR1 & AF) != 0 ) {
        return -2;
    }

    // resetting all status bits
    I2C1_SR1 = 0;
    (void)I2C1_SR2;
    (void)I2C1_SR1;

    return 0;
}

int i2cTxByteInner(uint8_t byte)
{
    int32_t tOut = I2C_TOUT;
    while(((I2C1_SR1 & ITXE) == 0) && ((I2C1_SR1 & AF) == 0) && (--tOut > 0));
    if(tOut <= 0) {
        return -1;
    }
    if((I2C1_SR1 & AF) != 0) {
        return -2;
    }
    I2C1_DR = byte;

    return 0;
}

int i2cRxByteInner(void)
{
    int32_t tOut = I2C_TOUT;
    while( ((I2C1_SR1 & IRXNE) == 0 ) && (--tOut > 0) );
    if(tOut <= 0) {
        return -1;
    }
    return (uint8_t)I2C1_DR;
}

int i2cTxStop(void)
{
    int32_t tOut = I2C_TOUT;
    while((((I2C1_SR1 & BTF) == 0) || ((I2C1_SR1 & ITXE) == 0)) && (--tOut > 0));
    I2C1_CR1 |= STOP;
    if(tOut <= 0) {
        return -1;
    }
    return 0;
}

int i2cTxByte(uint8_t addr, uint8_t data)
{
    int ret = i2cTxAddrInner(addr);
    ret += i2cTxByteInner(data);
    ret += i2cTxStop();
    return ret;
}

int i2cTxArray(uint8_t addr, uint8_t *data, int size)
{
    int ret = i2cTxAddrInner(addr);
    for(int i=0 ; i<size ; ++i) {
        ret += i2cTxByteInner(data);
    }
    ret += i2cTxStop();
    return ret;
}

int i2cRxByte(uint8_t addr)
{
    i2cTxAddrInner(addr);
    int ret += i2cRxByteInner();
    i2cTxStop();
    return ret;
}

int i2cRxArray(uint8_t addr, uint8_t *data, int size)
{
    int ret;
    i2cTxAddrInner(addr);
    for(int i=0 ; i<size ; ++i) {
        ret = i2cRxByteInner(data);
        if(ret<0)   return ret;
        data[i] = (uint8_t)ret;
    }
    ret = i2cTxStop();
    return ret;
}
