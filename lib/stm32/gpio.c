/*
 * Part of lowlevellib - a heap of microcontroller periferial code.
 * Here is STM32 GPIO API functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gpio.h"

void gpioToDefault(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_FLOATING(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_FLOATING(i);
        }
    }
}

void gpioSetInput(uint32_t port, uint32_t pin)
{
    gpioToDefault(port, pin);
}

void gpioSetAnalogue(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_ANALOG(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_ANALOG(i);
        }
    }
}

void gpioSetPushPull(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_PUSH_PULL(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        }
    }
}

void gpioSetOpenDrain(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_OPEN_DRAIN(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_OPEN_DRAIN(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        }
    }
}

void gpioSetAlternativeF(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( ((pin & (1<<i)) != 0) && ((GPIOx_CRL(port) & CNF_MASK(i)) == CNF_PUSH_PULL(i)) ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        } else if( ((pin & (1<<i)) != 0) && ((GPIOx_CRL(port) & CNF_MASK(i)) == CNF_OPEN_DRAIN(i)) ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_OPEN_DRAIN(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        } else if((pin & (1<<i)) != 0) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( ((pin & (1<<i)) != 0) && ((GPIOx_CRH(port) & CNF_MASK(i)) == CNF_PUSH_PULL(i)) ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        } else if( ((pin & (1<<i)) != 0) && ((GPIOx_CRH(port) & CNF_MASK(i)) == CNF_OPEN_DRAIN(i)) ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_AF_OPEN_DRAIN(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        } else if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        }
    }
}

void gpioSetPullUp(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUPD(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_PUPD(i);
        }
    }
    GPIOx_BSRR(port) = pin;
}

void gpioSetPullDown(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUPD(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRH(port) |= CNF_PUPD(i);
        }
    }
    GPIOx_BRR(port) = pin;
}

void gpioSetOutput2M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT2(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK(i);
            GPIOx_CRH(port) |= MODE_OUTPUT2(i);
        }
    }
}

void gpioSetOutput10M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT10(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK(i);
            GPIOx_CRH(port) |= MODE_OUTPUT10(i);
        }
    }
}

void gpioSetOutput50M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK(i);
            GPIOx_CRH(port) |= MODE_OUTPUT50(i);
        }
    }
}

void gpioSet(uint32_t port, uint32_t pin)
{
    GPIOx_BSRR(port) = pin;
}

void gpioReset(uint32_t port, uint32_t pin)
{
    GPIOx_BRR(port) = pin;
}

int gpioIsActive(uint32_t port, uint32_t pin)
{
    if( (GPIOx_ODR(port) & pin) == 0 ) {
        return 0;
    } else {
        return 1;
    }
    return 0;
}

uint32_t getPort(uint32_t port)
{
    return GPIOx_ODR(port);
}
