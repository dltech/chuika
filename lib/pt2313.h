#ifndef H_PT2313
#define H_PT2313
/*
 * Part of chuika - cheapest tourists radio.
 * Here is digital filter pt2313 functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "pt2313_reg.h"

#define DEFAULT_SWITCH  STEREO1 | LOUDNESS_OFF | GAIN0DB

int pt2313WriteReg(uint8_t reg);

void pt2313Init()

void pt2313Bass(int db);
void pt2313Treble(int db);
void pt2313Att(int speaker, int percent);
void pt2313Volume(int percent);
void pt2313Mute(void);
void pt2313MuteOff(void);
void pt2313InputSelect(int number, uint8_t gain);

#endif
