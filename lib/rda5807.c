/*
 * Part of chuika - cheapest tourists radio.
 * RDA5807 digital rf chip access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../stm32/i2c.h"

rdaRegTyp txRegs[TX_REGS];
rdaRegTyp rxRegs[RX_REGS];

// initial default config
void rdaInitConfig()
{
    txRegs[REG02H].reg = DHIZ | RCLK_MODE | RCLK_DIRECT | SEEKUP | \
                         CLK_MODE_24M | RDS_EN | NEW_METHOD | ENABLE;
    txRegs[REG03H].reg = BAND_WIDE | SPACE_25K;
    txRegs[REG04H].reg = DE;
    txRegs[REG05H].reg = ((0x8 << SEEKTH_SHIFT) & SEEKTH_MASK) | \
                         LNA_PORT_SEL_LNAP | LNA_ICSEL_BIT3M | \
                         (0x8 & VOLUME_MASK);
    txRegs[REG06H].reg = 0x0000;
    txRegs[REG07H].reg = ((0x10 << TH_SOFRBLEND_SHIFT) & TH_SOFRBLEND_MASK) | \
                         SOFTBLEND_EN;
    txRegs[REG08H].reg = 0x0000;
}

// some default parameters
void rdaDefaultParameters()
{
    txRegs[3] |= LNA_PORT_SEL_LNAP;
}

int rdaUpdateConfig()
{
    rdaDefaultParameters();
    uint8_t txBuffer[TX_REGS*2];
    for(int i=0 ; i<TX_REGS ; ++i) {
        txBuffer[i*2] = txRegs[i].hiByte;
        txBuffer[(i*2)+1] = txRegs[i].lowByte;
    }
    int ret = i2cTxArray(RDA_ADDRESS, txBuffer, TX_REGS*2);
    txRegs[REG02H].reg &= ~SOFT_RESET;
    txRegs[REG04H].reg &= ~RDS_FIFO_CLR;
    return ret;
}

int rdaRead()
{
    uint8_t rxBuffer[RX_REGS*2];
    int ret = i2cRxArray(RDA_ADDRESS, rxBuffer, RX_REGS*2);
    for(int i=0 ; i<RX_REGS ; ++i) {
        rxRegs[i].hiByte = rxBuffer[i*2];
        rxRegs[i].lowByte = rxBuffer[(i*2)+1];
    }
    if(rxRegs[REG0AH].reg & (STC + SF)) {
        txRegs[REG02H].reg &= ~SEEK;
        txRegs[REG03H].reg &= ~TUNE;
    }
    return ret;
}

// one bit properties
void rdaOutputHiz(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg &= ~(DHIZ);
    } else {
        txRegs[REG02H].reg |= DHIZ;
    }
}

void rdaMute(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg &= ~(DMUTE);
    } else {
        txRegs[REG02H].reg |= DMUTE;
    }
}

void rdaMono(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= MONO;
    } else {
        txRegs[REG02H].reg &= ~(MONO);
    }
}

void rdaBass(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= BASS;
    } else {
        txRegs[REG02H].reg &= ~(BASS);
    }
}

void rdaExternalClocking()
{
    txRegs[REG02H].reg &= ~RCLK_MODE;
    txRegs[REG02H].reg |= RCLK_DIRECT;
}

void rdaCalibrate(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg &= ~(RCLK_MODE);
    } else {
        txRegs[REG02H].reg = RCLK_MODE;
    }
}

void rdaQuartz(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg &= ~(RCLK_DIRECT);
    } else {
        txRegs[REG02H].reg = RCLK_DIRECT;
    }
}

void rdaSeekDir(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= SEEKUP;
    } else {
        txRegs[REG02H].reg &= ~(SEEKUP);
    }
}

void rdaSeek(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= SEEK;
    } else {
        txRegs[REG02H].reg &= ~(SEEK);
    }
}

void rdaEndlessSeek(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg &= ~(SKMODE);
    } else {
        txRegs[REG02H].reg |= SKMODE;
    }
}

void rdaRdsEn(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= RDS_EN;
    } else {
        txRegs[REG02H].reg &= ~(RDS_EN);
    }
}

void rdaNewDemodulator(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= NEW_METHOD;
    } else {
        txRegs[REG02H].reg &= ~(NEW_METHOD);
    }
}

void rdaReset(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= SOFT_RESET;
    } else {
        txRegs[REG02H].reg &= ~(SOFT_RESET);
    }
}

void rdaPowerOn(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG02H].reg |= ENABLE;
    } else {
        txRegs[REG02H].reg &= ~(ENABLE);
    }
}

void rdaTune(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG03H].reg |= TUNE;
    } else {
        txRegs[REG03H].reg &= ~(TUNE);
    }
}

void rdaBand50_65()
{
    txRegs[REG03H].reg &= ~BAND_MASK;
    txRegs[REG03H].reg |= BAND_SU;
    txRegs[REG07H] &= ~MODE_65M_50M;
}

void rdaBand65_76()
{
    txRegs[REG03H].reg &= ~BAND_MASK;
    txRegs[REG03H].reg |= BAND_SU;
    txRegs[REG07H].reg |= MODE_65M_50M;
}

void rdaBand76_91()
{
    txRegs[REG03H].reg &= ~BAND_MASK;
    txRegs[REG03H].reg |= BAND_JP;
}

void rdaBand87_108()
{
    txRegs[REG03H].reg &= ~BAND_MASK;
    txRegs[REG03H].reg |= BAND_EU;
}

void rdaBand76_108()
{
    txRegs[REG03H].reg &= ~BAND_MASK;
    txRegs[REG03H].reg |= BAND_WIDE;
}

void rdaRbds(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg |= RBDS;
    } else {
        txRegs[REG04H].reg &= ~(RBDS);
    }
}

void rdaRdsFifoMode(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg |= RDS_FIFO_EN;
    } else {
        txRegs[REG04H].reg &= ~(RDS_FIFO_EN);
    }
}

void rdaDeEmphasis50(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg |= DE;
    } else {
        txRegs[REG04H].reg &= ~(DE);
    }
}

void rdaRdsFifoClear(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg |= RDS_FIFO_CLR;
    } else {
        txRegs[REG04H].reg &= ~(RDS_FIFO_CLR);
    }
}

void rdaMute2(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg |= SOFTMUTE_EN;
    } else {
        txRegs[REG04H].reg &= ~(SOFTMUTE_EN);
    }
}

void rdaAfc(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG04H].reg &= ~(AFCD);
    } else {
        txRegs[REG04H].reg |= AFCD;
    }
}

void rdaOldSeekMode(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG05H].reg |= SEEK_MODE;
    } else {
        txRegs[REG05H].reg &= ~(SEEK_MODE_MASK);
    }
}

void rdaClockInvert(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG06H].reg |= WS_I_EDGE;
    } else {
        txRegs[REG06H].reg &= ~(WS_I_EDGE);
    }
}

void rdaFilter(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG07H].reg |= SOFTBLEND_EN;
    } else {
        txRegs[REG07H].reg &= ~(SOFTBLEND_EN);
    }
}

void rdaFreqDirectMode(uint8_t onOff)
{
    if(onOff) {
        txRegs[REG07H].reg |= FREQ_MODE;
    } else {
        txRegs[REG07H].reg &= ~(FREQ_MODE);
    }
}


// yet another bitfields
void rdaClkFreq(int kHz)
{
    txRegs[REG02H].reg &= ~CLK_MODE_MASK;
    if(kHz <= 33) {
        txRegs[REG02H].reg |= CLK_MODE_32768;
    } else if(kHz < 12500) {
        txRegs[REG02H].reg |= CLK_MODE_12M;
    } else if(kHz < 14000) {
        txRegs[REG02H].reg |= CLK_MODE_13M;
    } else if(kHz < 20000) {
        txRegs[REG02H].reg |= CLK_MODE_19P2M;
    } else if(kHz < 25000) {
        txRegs[REG02H].reg |= CLK_MODE_24M;
    } else if(kHz < 27000) {
        txRegs[REG02H].reg |= CLK_MODE_27M;
    } else if(kHz < 39000) {
        txRegs[REG02H].reg |= CLK_MODE_38P4M;
    }
}

void rdaChannelSpacing(int kHz)
{
    txRegs[REG03H].reg &= ~SPACE_MASK;
    if(kHz < 30) {
        txRegs[REG03H].reg |= SPACE_25K;
    } else if(kHz < 60) {
        txRegs[REG03H].reg |= SPACE_50K;
    } else if(kHz < 110) {
        txRegs[REG03H].reg |= SPACE_100K;
    } else if(kHz < 210) {
        txRegs[REG03H].reg |= SPACE_200K;
    }
}

void rdaChannelN(uint16_t n)
{
    txRegs[REG03H].reg &= ~CHAN_MASK;
    txRegs[REG03H].reg |= (n << CHAN_SHIFT) & CHAN_MASK;
}

void rdaChannelKhz(uint32_t khz)
{
    txRegs[REG03H].reg &= ~CHAN_MASK;
    uint8_t spacing = 0;
    uint32_t lowFreq = 0;
    if( (txRegs[REG03H].reg & SPACE_MASK) == SPACE_25K ) {
        spacing = 25;
    }
    if( (txRegs[REG03H].reg & SPACE_MASK) == SPACE_50K ) {
        spacing = 50;
    }
    if( (txRegs[REG03H].reg & SPACE_MASK) == SPACE_100K ) {
        spacing = 100;
    }
    if( (txRegs[REG03H].reg & SPACE_MASK) == SPACE_200K ) {
        spacing = 200;
    }

    if( ((txRegs[REG03H].reg & BAND_MASK) == BAND_SU ) && ((txRegs[REG07H].reg & MODE_65M_50M) > 0) ) {
        lowFreq = 65000;
    }
    if( ((txRegs[REG03H].reg & BAND_MASK) == BAND_SU ) && ((txRegs[REG07H].reg & MODE_65M_50M) == 0) ) {
        lowFreq = 50000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_JP ) {
        lowFreq = 76000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_EU ) {
        lowFreq = 87000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_WIDE ) {
        lowFreq = 76000;
    }
    txRegs[REG03H].reg |= (((khz - lowFreq) / spacing) << CHAN_SHIFT) & CHAN_MASK;
}

void rdaFreqDirect(uint32_t khz)
{
    uint32_t lowFreq = 0;
    if( ((txRegs[REG03H].reg & BAND_MASK) == BAND_SU ) && ((txRegs[5] & MODE_65M_50M) > 0) ) {
        lowFreq = 65000;
    }
    if( ((txRegs[REG03H].reg & BAND_MASK) == BAND_SU ) && ((txRegs[5] & MODE_65M_50M) == 0) ) {
        lowFreq = 50000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_JP ) {
        lowFreq = 76000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_EU ) {
        lowFreq = 87000;
    }
    if((txRegs[REG03H].reg & BAND_MASK) == BAND_WIDE ) {
        lowFreq = 76000;
    }
    txRegs[REG08H].reg = (uint16_t)(khz - lowFreq);
}

void rdaSeekThreshold(uint8_t snr)
{
    txRegs[REG05H].reg &= ~SEEKTH_MASK;
    txRegs[REG05H].reg |= (snr << SEEKTH_SHIFT) & SEEKTH_MASK;
}

void rdaLnaCurrentUa(uint16_t current)
{
    txRegs[REG05H].reg &= ~LNA_ICSEL_BITMASK;
    if(current < 1900) {
        txRegs[REG05H].reg |= LNA_ICSEL_BIT1P8M;
    } else if(current < 2200) {
        txRegs[REG05H].reg |= LNA_ICSEL_BIT2P1M;
    } else if(current < 2600) {
        txRegs[REG05H].reg |= LNA_ICSEL_BIT2P5M;
    } else if(current < 3100) {
        txRegs[REG05H].reg |= LNA_ICSEL_BIT3M;
    }
}

void rdaVolume(uint8_t vol)
{
    txRegs[REG05H].reg &= ~VOLUME_MASK;
    txRegs[REG05H].reg |= vol & VOLUME_MASK;
}

void rdaOldSeekThreshold(uint8_t snr)
{
    txRegs[REG07H].reg &= ~SEEK_TH_OLD_MASK;
    txRegs[REG07H].reg |= (snr << SEEK_TH_OLD_SHIFT) & SEEK_TH_OLD_MASK;
}

void rdaSoftBlendThresholdDb(uint8_t db)
{
    txRegs[REG07H].reg &= ~TH_SOFRBLEND_MASK;
    txRegs[REG07H].reg |= ((db/2) << TH_SOFRBLEND_SHIFT) & TH_SOFRBLEND_MASK;
}

// getters for readed parameters

int isRdaRdsReady()
{
    if(rxRegs[REG0AH].reg & RDSR) {
       return 1;
    }
    return 0;
}

int isRdaSeekComplete()
{
    if(rxRegs[REG0AH].reg & STC) {
       return 1;
    }
    return 0;
}

int isRdaSeekFail()
{
    if(rxRegs[REG0AH].reg & SF) {
       return 1;
    }
    return 0;
}

int isRdaRdsSynchronized()
{
    if(rxRegs[REG0AH].reg & RDSS) {
       return 1;
    }
    return 0;
}

int isRdaStereo()
{
    if(rxRegs[REG0AH].reg & ST) {
       return 1;
    }
    return 0;
}

uint16_t rdaGetChannel()
{
    return rxRegs[REG0AH].reg & READCHAN_MSK;
}

uint8_t rdaGetRssi()
{
    return (rxRegs[REG0BH].reg & RSSI_MASK) >> RSSI_SHIFT;
}

int isRdaRdsTrueStation()
{
    if(rxRegs[REG0BH].reg & FM_TRUE) {
       return 1;
    }
    return 0;
}

int isRdaRdsReady()
{
    if(rxRegs[REG0BH].reg & FM_READY) {
       return 1;
    }
    return 0;
}

int rdaGetRdsErrors()
{
    int err[2] = {0};
    err[0] = (rxRegs[REG0BH].reg & BLERA_MASK) >> BLERA_SHIFT;
    err[1] = rxRegs[REG0BH].reg & BLERB_MASK;
    int summ = 0;
    for(int i=0 ; i<2 ; ++i) {
        if(err[i] == 1) {
            summ += 2;
        }
        if(err[i] == 2) {
            summ += 5;
        }
        if(err[i] == 3) {
            summ += 6;
        }
    }
    return summ;
}

void rdaGetRds(uint16_t *rds)
{
    for(int i=0 ; i<RDS_BLOCK_SIZE ; ++i) {
        rds[i] = (rxRegs[REG0CH+i].hiByte << 8) + rxRegs[REG0CH+i].lowByte;
    }
}
