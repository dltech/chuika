# chuika

## Name
chuika

## Description
Cheapest FM radio module with mp3, thermometer, alarm clock, 2x2W class D power amp, line output, RGB disco lights. 

## Visuals
prewiev of a board (5.2x5.5cm)\
![eee](pcb/prerender.png)
schematics diadram\
![eee](result/pcb.png)

## Building
If you want to build again a project you need gcc-arm-none-eabi compiler. After installing compiler run make in chuika subdirectory.
For a board editing, install Kicad with the latest libraries. Board files are in pcb subdirectory, but you may need a library from datasheet repository.

## Installation
A PCB in kicad, a firmware in hex. Board can be made at home, for uploading firmware you need st-link stick.
All nesessary files are in result directory (A4 pcb templates in svg, hex and binary).

## Usage
Connect speakers. Push play button! Turn volume encoder!

## Roadmap
*Bluetooth version maybe.

## License
no copyleft apache licence

## Project status
Hope to finish it until march, sorry, april